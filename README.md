# Aplikasi Customer #

## Membuat database user ##

1. Login dulu ke MySQL Server

    ```
    mysql -u root
    ```

2. Buat user untuk mengakses database

    ```
    grant all on namadb.* to namauser@localhost identified by 'passwordnya';
    ```

    misalnya

    ```
    grant all on customerdb.* to trainingdevops@localhost identified by 'devops201901';
    ```


3. Buat databasenya

    ```
    create database customerdb;
    ```

## Menjalankan Aplikasi ##

1. Masuk ke folder aplikasi

    ```
    cd aplikasi-customer
    ```

2. Jalankan lewat Maven

    ```
    mvn clean spring-boot:run
    ```

3. Insert data dengan method `POST` ke `http://localhost:8080/customer/` dengan membawa payload berikut:

    ```js
    {
        "fullname" : "Endy Muhardin",
        "email" : "endy@muhardin.com",
        "mobilePhone" : "08123456789"
    }
    ```

    atau dengan Postman seperti ini

    [![Postman Insert](docs/img/01-postman-insert.png)](docs/img/01-postman-insert.png)

4. Lihat data seluruh customer dengan method `GET` ke `http://localhost:8080/customer/`. Outputnya seperti ini

    ```js
    {
        "content": [
            {
                "id": "01934f58-9bac-43a7-a076-3760498942f3",
                "fullname": "Endy Muhardin",
                "email": "endy@muhardin.com",
                "mobilePhone": "08123456789"
            }
        ],
        "pageable": {
            "sort": {
                "sorted": false,
                "unsorted": true,
                "empty": true
            },
            "offset": 0,
            "pageSize": 20,
            "pageNumber": 0,
            "paged": true,
            "unpaged": false
        },
        "last": true,
        "totalPages": 1,
        "totalElements": 1,
        "size": 20,
        "number": 0,
        "numberOfElements": 1,
        "first": true,
        "sort": {
            "sorted": false,
            "unsorted": true,
            "empty": true
        },
        "empty": false
    }
    ```

    atau dengan Postman seperti ini

    [![Postman Find All](docs/img/02-postman-find-all.png)](docs/img/02-postman-find-all.png)

5. Lihat data satu customer dengan method `GET` ke `http://localhost:8080/customer/{id}`. Outputnya seperti ini

    ```js
    {
        "id": "01934f58-9bac-43a7-a076-3760498942f3",
        "fullname": "Endy Muhardin",
        "email": "endy@muhardin.com",
        "mobilePhone": "08123456789"    
    }
    ```

    atau dengan Postman seperti ini

    [![Postman Find Id](docs/img/03-postman-find-id.png)](docs/img/03-postman-find-id.png)