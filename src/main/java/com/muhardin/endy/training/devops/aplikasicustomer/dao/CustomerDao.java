package com.muhardin.endy.training.devops.aplikasicustomer.dao;

import com.muhardin.endy.training.devops.aplikasicustomer.entity.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String> {
    
}
